const express = require("express");
const mongoose = require("mongoose");

const app = express();
const port = 4004;

// Allows us to read json data
app.use(express.json());

// Allows us to read data from forms
app.use(express.urlencoded({extended:true}));

	// [ SECTION ] MongoDB Connection
	// Connect to the database by passing in your connection string, remember to replace the password

	// Syntax:
		// mongoose.connect("MongoDB connection string", { useNewUrlParser : true });

// Connecting to MongoDB Atlas
mongoose.connect("mongodb+srv://admin1:admin123@zuitt-bootcamp1.l5iqt9p.mongodb.net/?retryWrites=true&w=majority", 
	{
		useNewUrlParser : true,
		useUnifiedTopology : true
	}
);

let db = mongoose.connection;

db.on("error", console.error.bind(console, "connection error"));

db.on("open", () => console.log("We're connected to the cloud database"));


// [ SECTION ] Moongoose Schemas
	// Schemas determine the structure of the documents to be written in the database
	// Schemas act as blueprints to our data
	// Use the Schema() constructor of the Mongoose module to create a new Schema object
	// The "new" keyword creates a new Schema
/*
const taskSchema = new mongoose.Schema({
	// Define the fields with the corresponding data type
	name : String,
	status : {
		type : String,
		// Default values are the predefined values for a field if we don't put any value
		default : "pending"
	}
})
*/

// [ SECTION ] Models
	// Uses schemas and are used to create/instantiate objects that correspond to the schema
	// Server > Schema (blueprint) > Database > Collection

// MongoDB Model should always be Capital & Singular
//const Task = mongoose.model("Task", taskSchema);

// [ SECTION ] Routes

// Business Logic
/*
	1. Add a functionality to check if there are duplicate tasks
		- If the task already exists in the database, we return an error
		- If the task doesn't exist in the database, we add in the the database
	2. The task data will be coming from thre request's body
	3. Create a new Task object with a "name" field/property
	4. The "status" property does not need to be provided because our schema defaults it to "pending" upon creation of an object.



app.post("/tasks", (request, response) => {
	
	// Checks if there are duplicate tasks
	Task.findOne({name: request.body.name}, (error, result) => {
		if(result != null && result.name == request.body.name){
			return response.send('Duplicate task found!')
		} 

		let newTask = new Task({
			name: request.body.name
		})

		newTask.save((error, savedTask) => {
			if(error){
				return console.error(error)
			} else {
				return response.status(200).send('New task created!')
			}
		})
	})
})

// Business Logic
/*

1. Retrieve all the documents
2. If an error is encountered, print error
3. If no errors are found, send a success status back to client/postman and return an array of documents

*/

/*app.get("/tasks", (req, res) => {

	// "find" is a Mongoose Method that is similar to MongoDB 'find', and empty "{}" means it returns all the documents and stores them in the "result" parameter of the callback function
	Task.find({}, (err, result) => {

		// If an error occured
		if (err) {

			// To print any erros found in the console
			return console.log(err);

		// If no error are found
		} else {

			// Return the result
			// The "json" method allows to send a JSON format for the response
			// The returned response is purposefully returened as an object with the "data" property to mirror real world complex data structures
			return res.status(200).json({
				data : result
			})
		}
	})
})

*/

//Activity S35

//1. Create a User schema.
//2. Create a User model.
//3. Create a POST route that will access the /signup route that will create a user.
//4. Process a POST request at the /signup route using postman to register a user.
//5. Create a git repository named s35.
//6. Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.
//7. Add the link in Boodle.

const signupSchema = new mongoose.Schema({
		username : String,
		password: String
		
	})


	const Task = mongoose.model("Task", signupSchema);

app.post("/signup", (request, response) => {
		
		// Checks if there are duplicate tasks
		Task.findOne({username: request.body.username}, (error, result) => {
			if(result != null && result.name == request.body.name){
				return response.send('Duplicate username found!')

			} 

			let newTask = new Task({
				username: request.body.username,
				password: request.body.password
			})

			newTask.save((error, savedTask) => {
				if(error){
					return console.error(error)
				} else {
					return response.status(200).send('New user registered!')
				}
			})
		})
	})

// Listen to the port
app.listen(port, () => console.log(`Server is now running at port ${port}`));